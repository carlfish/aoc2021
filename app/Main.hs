module Main where

import Criterion.Main ( defaultMain, bench, bgroup, nfIO )
import qualified Day01
import qualified Day02
import qualified Day03
import qualified Day04
import qualified Day05
import qualified Day06
import qualified Day07
import qualified Day08

main :: IO ()
main = defaultMain
  [ bgroup "day01"
    [ bench "1" $ nfIO Day01.star1 
    , bench "2" $ nfIO Day01.star2
    ]
  , bgroup "day02"
    [ bench "1" $ nfIO (Day02.star1 "problem.txt")
    , bench "2" $ nfIO (Day02.star2 "problem.txt")
    ]
  , bgroup "day03"
    [ bench "1" $ nfIO (Day03.star1 "problem.txt")
    , bench "2" $ nfIO (Day03.star2 "problem.txt")
    ]
  , bgroup "day04"
    [ bench "1" $ nfIO (Day04.star1 "problem.txt")
    , bench "2" $ nfIO (Day04.star2 "problem.txt")
    ]
  , bgroup "day05"
    [ bench "1" $ nfIO (Day05.star1 "problem.txt")
    , bench "2" $ nfIO (Day05.star2 "problem.txt")
    ]
  , bgroup "day06"
    [ bench "1" $ nfIO (Day06.star1 "problem.txt")
    , bench "2" $ nfIO (Day06.star2 "problem.txt")
    ]
  , bgroup "day06alt"
    [ bench "1" $ nfIO (Day06.star1' "problem.txt")
    , bench "2" $ nfIO (Day06.star2' "problem.txt")
    ]
  , bgroup "day07"
    [ bench "1" $ nfIO (Day07.star1 "problem.txt")
    , bench "2" $ nfIO (Day07.star2 "problem.txt")
    ]
  , bgroup "day08"
    [ bench "1" $ nfIO (Day08.star1 "problem.txt")
    , bench "2" $ nfIO (Day08.star2 "problem.txt")
    ]
  ]