# advent2020

Charles attempts at Advent of Code 2021.

## Solutions

* [Day 1 (both stars)](src/Day01.hs)
* [Day 2 (both stars)](src/Day02.hs)
* [Day 3 (both stars)](src/Day03.hs)
* [Day 4 (both stars)](src/Day04.hs)
* [Day 5 (both stars)](src/Day05.hs)
* [Day 6 (both stars)](src/Day06.hs)
* [Day 7 (both stars)](src/Day07.hs)
* [Day 8 (both stars)](src/Day08.hs)

# Notes

I'm not here to play code golf or win any speed prizes. I'm here to have fun solving problems in
a way that is pleasing to my own brain.

All solutions are my own work, but after I've solved a problem myself I will often look at other 
peoples solutions, and might bring some of their good ideas back to my code.