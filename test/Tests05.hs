module Tests05 (day5tests) where

import Test.HUnit (Test, test, (~:))
import TestLib (assertAnswer)
import qualified Day05

day5tests :: Test
day5tests = test 
  [ "test1" ~: assertAnswer (Day05.star1 "test.txt") 5
  , "star1" ~: assertAnswer (Day05.star1 "problem.txt") 6005
  , "test2" ~: assertAnswer (Day05.star2 "test.txt") 12
  , "star2" ~: assertAnswer (Day05.star2 "problem.txt") 23864
  ]