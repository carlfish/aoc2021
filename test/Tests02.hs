module Tests02 (day2tests) where

import Test.HUnit (Test, test, (~:))
import TestLib (assertAnswer)
import qualified Day02

day2tests :: Test
day2tests = test 
  [ "test1" ~: assertAnswer (Day02.star1 "test.txt") 150
  , "test2" ~: assertAnswer (Day02.star2 "test.txt") 900
  , "star1" ~: assertAnswer (Day02.star1 "problem.txt") 1488669
  , "star2" ~: assertAnswer (Day02.star2 "problem.txt") 1176514794
  ]