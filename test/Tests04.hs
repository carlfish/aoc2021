module Tests04 (day4tests) where

import Test.HUnit (Test, test, (~:))
import TestLib (assertAnswer)
import qualified Day04

day4tests :: Test
day4tests = test 
  [ "test1" ~: assertAnswer (Day04.star1 "test.txt") 4512
  , "star1" ~: assertAnswer (Day04.star1 "problem.txt") 45031
  , "test2" ~: assertAnswer (Day04.star2 "test.txt") 1924
  , "star2" ~: assertAnswer (Day04.star2 "problem.txt") 2568
  ]