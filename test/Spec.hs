import Test.HUnit

import Tests01 (day1tests)
import Tests02 (day2tests)
import Tests03 (day3tests)
import Tests04 (day4tests)
import Tests05 (day5tests)
import Tests06 (day6tests)
import Tests07 (day7tests)
import Tests08 (day8tests)


main :: IO ()
main = runTestTTAndExit tests

tests :: Test
tests = test
  [ "Day 1" ~: day1tests
  , "Day 2" ~: day2tests
  , "Day 3" ~: day3tests
  , "Day 4" ~: day4tests
  , "Day 5" ~: day5tests
  , "Day 6" ~: day6tests
  , "Day 7" ~: day7tests
  , "Day 8" ~: day8tests
  ]