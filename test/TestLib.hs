module TestLib (assertAnswer) where

import Test.HUnit

assertAnswer :: (Show a, Eq a) => IO (Either String a) -> a -> Assertion
assertAnswer f v = f >>= (@=?) (Right v)