module Tests01 (day1tests) where

import Test.HUnit (Test, test, (~:), (~=?))
import TestLib (assertAnswer)
import qualified Day01

testList :: [Int]
testList = 
  [ 199
  , 200
  , 208
  , 210
  , 200
  , 207
  , 240
  , 269
  , 260
  , 263 ]

day1tests :: Test
day1tests = test 
  [ "offset 1" ~: (Day01.countIncsOffset 1 testList ~=? 7)
  , "offset 3" ~: (Day01.countIncsOffset 3 testList ~=? 5)
  , "star1" ~: assertAnswer Day01.star1 1529
  , "star2" ~: assertAnswer Day01.star2 1567
  ]