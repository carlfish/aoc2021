module Tests03 (day3tests) where

import Test.HUnit (Test, test, (~:))
import TestLib (assertAnswer)
import qualified Day03

day3tests :: Test
day3tests = test 
  [ "test1" ~: assertAnswer (Day03.star1 "test.txt") 198
  , "test2" ~: assertAnswer (Day03.star2 "test.txt") 230
  , "star1" ~: assertAnswer (Day03.star1 "problem.txt") 3309596
  , "star2" ~: assertAnswer (Day03.star2 "problem.txt") 2981085
  ]