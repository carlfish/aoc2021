module Tests08 (day8tests) where

import Test.HUnit (Test, test, (~:))
import TestLib (assertAnswer)
import qualified Day08

day8tests :: Test
day8tests = test 
  [ "test1" ~: assertAnswer (Day08.star1 "test.txt") 26
  , "star1" ~: assertAnswer (Day08.star1 "problem.txt") 344
  , "test2" ~: assertAnswer (Day08.star2 "test.txt") 61229
  , "star2" ~: assertAnswer (Day08.star2 "problem.txt") 1048410
  ]