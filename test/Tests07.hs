module Tests07 (day7tests) where

import Test.HUnit (Test, test, (~:))
import TestLib (assertAnswer)
import qualified Day07

day7tests :: Test
day7tests = test 
  [ "test1" ~: assertAnswer (Day07.star1 "test.txt") 37
  , "star1" ~: assertAnswer (Day07.star1 "problem.txt") 339321
  , "test2" ~: assertAnswer (Day07.star2 "test.txt") 168
  , "star2" ~: assertAnswer (Day07.star2 "problem.txt") 95476244
  -- , "star2a" ~: assertAnswer (Day07.star2A "problem.txt") 95476244
  ]