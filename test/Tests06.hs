module Tests06 (day6tests) where

import Test.HUnit (Test, test, (~:))
import TestLib (assertAnswer)
import qualified Day06

day6tests :: Test
day6tests = test 
  [ "test1" ~: assertAnswer (Day06.star1 "test.txt") 5934
  , "star1" ~: assertAnswer (Day06.star1 "problem.txt") 365131
  , "test2" ~: assertAnswer (Day06.star2 "test.txt") 26984457539
  , "star2" ~: assertAnswer (Day06.star2 "problem.txt") 1650309278600
  , "star2'" ~: assertAnswer (Day06.star2' "problem.txt") 1650309278600
  ]