module Day04 where

import Lib (runFile')
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Data.List (partition)
import Control.Monad (join)

type Call = Int
type Calls = [ Call ]
type Board = [ [ Int ] ]
type Boards = [ Board ]
type Win = (Calls, Board)

data Game = Game Calls Boards

boardSize :: Int
boardSize = 5

boardWins :: Calls -> Board -> Bool
boardWins cs board =
  let
    lineWins calls line = all (`elem` calls) line
    linesToColumns b
      | null (head b) = []
      | otherwise = (head <$> b) : linesToColumns (tail <$> b)    

    linesWin   = any (lineWins cs)
    columnsWin = linesWin . linesToColumns
  in
    linesWin board || columnsWin board

winners :: Game -> [ Win ]
winners (Game calls boards) = 
  let 
    winners' _ _ [] = []
    winners' bs donecalls (thiscall : futurecalls) =
      let 
        currentcalls = (thiscall : donecalls) 
      in
        case partition (boardWins currentcalls) bs of
          (winningboards, remainingboards) ->
            ((currentcalls,) <$> winningboards) 
              ++ winners' remainingboards currentcalls futurecalls

    -- It takes at least boardSize calls to find the first winner.
    fastforward = boardSize - 1
  in
    winners' boards (take fastforward calls) (drop fastforward calls)

score :: Win -> Int
score (cs, b) = sum (filter (`notElem` cs) (join b)) * head cs

-- Parsers and problems (phew!)

parseGame :: AP.Parser Game
parseGame = 
  let
    paddedDecimal = AP.many' (AP.char ' ') *> AP.decimal
    parseCalls    = AP.sepBy1 AP.decimal (AP.char ',')
    parseLine     = AP.many1 paddedDecimal
    parseBoard    = AP.sepBy1 parseLine (AP.char '\n')
    parseBoards   = AP.sepBy1 parseBoard (AP.string "\n\n")
  in
    Game <$> parseCalls <* AP.string "\n\n" <*> parseBoards

star1, star2 :: String -> IO (Either String Int)
star1 fileName = runFile' ("data/04/" <> fileName) parseGame (score . head . winners)
star2 fileName = runFile' ("data/04/" <> fileName) parseGame (score . last . winners)