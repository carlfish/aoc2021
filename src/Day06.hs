module Day06 where

import Lib (runFile')
import qualified Data.Attoparsec.ByteString.Char8 as AP

-- No complicated data structure, about 5x slower

data Fish = Fish { generation :: Int, count :: Int}

oneDay' :: [ Fish ] -> [ Fish ]
oneDay' fishes =
  let
    growFish (Fish 0 c)   = [ Fish 8 c, Fish 6 c ]
    growFish (Fish gen c) = [ Fish (gen - 1) c ]
  in
    growFish =<< compress fishes

compress :: [ Fish ] -> [ Fish ]
compress fish =
  let
    counts n = sum (count <$> filter (\f -> generation f == n) fish)
  in
    fmap (\g -> Fish g (counts g)) [0..8]

solveFlatmap :: Int -> ([Int] -> Int)
solveFlatmap n = sum . fmap count . (!! n) . iterate oneDay' . fmap (`Fish` 1)

-- Overly complicated data structure but faster.

data Fishes = Fishes Int Int Int Int Int Int Int Int Int

oneDay :: Fishes -> Fishes
oneDay (Fishes d0 d1 d2 d3 d4 d5 d6 d7 d8) =
  Fishes d1 d2 d3 d4 d5 d6 (d7 + d0) d8 d0

countFishes :: Fishes -> Int
countFishes (Fishes d0 d1 d2 d3 d4 d5 d6 d7 d8) =
  d0 + d1 + d2 + d3 + d4 + d5 + d6 + d7 + d8

toFishes :: [ Int ] -> Fishes
toFishes is =
  let
    at n = length (filter (== n) is)
  in
    Fishes (at 0) (at 1) (at 2) (at 3) (at 4) (at 5) (at 6) (at 7) (at 8)

solveBuckets :: Int -> ([Int] -> Int)
solveBuckets n = countFishes . (!! n) . iterate oneDay . toFishes

-- Parsers and runners

parseFish :: AP.Parser [ Int ]
parseFish = AP.sepBy AP.decimal (AP.char ',')

star1, star2, star1', star2' :: String -> IO (Either String Int)
star1 fileName = runFile' ("data/06/" <> fileName) parseFish (solveBuckets 80)
star2 fileName = runFile' ("data/06/" <> fileName) parseFish (solveBuckets 256)
star1' fileName = runFile' ("data/06/" <> fileName) parseFish (solveFlatmap 80)
star2' fileName = runFile' ("data/06/" <> fileName) parseFish (solveFlatmap 256)