{-# LANGUAGE MultiParamTypeClasses #-}

module Lib
    ( parseFile
    , onePerLine
    , whitespace
    , sp
    , commaSeparated
    , range
    , runFile
    , runFile'
    , runFileBlankLineSeparated
    , runFileIO
    , runFileIO'
    , count
    , safeHead
    , testParser
    , tt
    , ttt
    ) where

import qualified Data.Attoparsec.ByteString.Char8 as AP

import Data.Attoparsec.ByteString.Char8 (Parser)
import System.IO (openFile, IOMode(..))
import Data.ByteString (ByteString, hGetContents)
import qualified Data.ByteString.Char8 as BC
import Data.Maybe (listToMaybe)
import Data.Either (rights)
import Control.Monad (void)
import Debug.Trace (trace)

-- Helpers

count :: (a -> Bool) -> [ a ] -> Int
count f = length . filter f

safeHead :: [ a ] -> Maybe a
safeHead = listToMaybe

tt :: Show a => a -> a
tt a = trace (show a) a

ttt :: Show a => String -> a -> a
ttt s a = trace (s <> ": " <> show a) a

-- simple range that works backwards as well as forwards
range :: (Enum a, Ord a) => a -> a -> [a]
range start end  = case compare start end of
  LT -> start : range (succ start) end
  EQ -> [ end ]
  GT -> start : range (pred start) end

-- Parsers

onePerLine :: Parser a ->Parser [ a ]
onePerLine p = AP.sepBy p AP.endOfLine

whitespace :: AP.Parser ()
whitespace = void (AP.takeWhile AP.isSpace)

sp :: AP.Parser ()
sp = whitespace

commaSeparated :: Parser a ->Parser [ a ]
commaSeparated p = AP.sepBy p (AP.char ',' <* whitespace)

loadFile :: FilePath -> IO ByteString
loadFile file = do
  handle <- openFile file ReadMode
  hGetContents handle

parseFile :: String -> Parser a -> IO (Either String a)
parseFile file parser = AP.parseOnly parser <$> loadFile file

splitFileOnBlankLines :: String -> IO [ ByteString ]
splitFileOnBlankLines file =
  let
    splitOnBlanks bs =
        h : if BC.null t then [] else splitOnBlanks (BC.drop 2 t)
             where (h,t) = BC.breakSubstring "\n\n" bs
  in
    splitOnBlanks <$> loadFile file

testParser :: Parser a -> ByteString -> Either String a
testParser p = AP.parseOnly (p <* AP.endOfInput)

-- Exercise runners

runStar :: IO ByteString -> Parser a -> (a -> IO (Either String b)) -> IO (Either String b)
runStar input parser processor = do
    p <- AP.parseOnly parser <$> input
    either (return . Left) processor p

runFileIO :: String -> Parser a -> (a -> IO (Either String b)) -> IO (Either String b)
runFileIO file = runStar (loadFile file)

runFileIO' :: String -> Parser a -> (a -> IO b) -> IO (Either String b)
runFileIO' file parser processor = runStar (loadFile file) parser (fmap pure . processor)

runFile :: String -> Parser a -> (a -> Either String b) -> IO (Either String b)
runFile file parser processor = runFileIO file parser (pure . processor)

runFile' :: String -> Parser a -> (a -> b) -> IO (Either String b)
runFile' file parser processor = runFile file parser (pure . processor)

runFileBlankLineSeparated :: String -> Parser a -> ([a] -> b) -> IO b
runFileBlankLineSeparated file parser processor =
  let parseSections = (processor . rights . fmap (AP.parseOnly parser))
  in parseSections <$> splitFileOnBlankLines file