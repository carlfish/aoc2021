module Day07 where

import Lib (runFile', runFileIO')
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Control.Concurrent.Async (mapConcurrently)

constantCost :: Int -> Int
constantCost = id

linearIncreasingCost :: Int -> Int 
linearIncreasingCost pos = pos * (pos + 1) `div` 2

minCost :: (Int -> Int) -> [ Int ] -> Int
minCost costfn crabs = 
  let 
    fuelCost pos = sum (costfn . abs . (pos-) <$> crabs)
  in
    minimum (fmap fuelCost [ 0 .. (maximum crabs) ])

-- minCostA :: (Int -> Int) -> [ Int ] -> IO Int
-- minCostA costfn crabs = 
--   let 
--     fuelCost pos = sum <$> mapConcurrently (pure. costfn . abs . (pos-)) crabs
--   in
--     minimum <$> sequence (fuelCost <$> [ 0 .. (maximum crabs) ])

-- Parser and runners

parseCrabs :: AP.Parser [ Int ]
parseCrabs = AP.sepBy AP.decimal (AP.char ',')

star1, star2 :: String -> IO (Either String Int)
star1 fileName = runFile' ("data/07/" <> fileName) parseCrabs (minCost constantCost)
star2 fileName = runFile' ("data/07/" <> fileName) parseCrabs (minCost linearIncreasingCost)
-- star1A fileName = runFileIO' ("data/07/" <> fileName) parseCrabs (minCostA constantCost)
-- star2A fileName = runFileIO' ("data/07/" <> fileName) parseCrabs (minCostA linearIncreasingCost)