module Day03 where

import Lib (runFile', onePerLine)
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Control.Applicative ((<|>))
import Data.Foldable (foldl')
import Data.Bits ( shift )

-- Bit handling 
data Bit = Zero | One 
  deriving (Eq, Show)

data BitFrequency = MoreZeros | MoreOnes | SameFrequency

btoi :: Bit -> Int
btoi Zero = 0
btoi One = 1

invert :: Bit -> Bit
invert Zero = One
invert One = Zero

bitstoi :: [ Bit ] -> Int
bitstoi = foldl' (\n b -> shift n 1 + btoi b) 0

firstBitFrequency :: [ [ Bit ] ] -> BitFrequency
firstBitFrequency = 
  let
    sumBit Zero = -1 :: Int
    sumBit One  = 1  :: Int
    toFrequency i
      | i < 0     = MoreZeros
      | i > 0     = MoreOnes
      | otherwise = SameFrequency
  in
    toFrequency . sum . fmap (sumBit . head)

-- Star 1

mostFrequentBits :: [ [ Bit ] ] -> [ Bit ]
mostFrequentBits bs 
  | null (head bs) = []
  | otherwise      = mostFrequentBit (firstBitFrequency bs) : mostFrequentBits (tail <$> bs)
  where
    mostFrequentBit f = case f of
      MoreOnes   -> One
      MoreZeros  -> Zero
      SameFrequency -> undefined

solveOne :: [ [ Bit ] ] -> Int
solveOne readings =
  let
    mfbs = mostFrequentBits readings
  in
    bitstoi mfbs * bitstoi (invert <$> mfbs)

-- Star 2

type BitSelector = BitFrequency -> Bit

oxyCriteria :: BitSelector
oxyCriteria f = case f of
  MoreZeros     -> Zero
  MoreOnes      -> One
  SameFrequency -> One

co2Criteria :: BitSelector
co2Criteria = invert . oxyCriteria

rating :: BitSelector -> [ [ Bit ] ] -> [ Bit ]
rating _ []  = []
rating _ [i] = i
rating selectBit readings =
  let
    candidateBit = selectBit (firstBitFrequency readings)
    hasCandidateBit = filter ((== candidateBit) . head)
  in
    candidateBit : rating selectBit (tail <$> hasCandidateBit readings)

solveTwo :: [ [ Bit ] ] -> Int
solveTwo bs
  = bitstoi (rating oxyCriteria bs)
  * bitstoi (rating co2Criteria bs)

-- Parser and plumbing

parseBit :: AP.Parser Bit
parseBit = Zero <$ AP.char '0' <|> One <$ AP.char '1'

parseBinary :: AP.Parser [ Bit ]
parseBinary = AP.many' parseBit

parseTraces :: AP.Parser [ [ Bit ] ]
parseTraces = onePerLine parseBinary

star1, star2 :: String -> IO (Either String Int)
star1 fileName = runFile' ("data/03/" <> fileName) parseTraces solveOne
star2 fileName = runFile' ("data/03/" <> fileName) parseTraces solveTwo