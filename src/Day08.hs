module Day08 where

import Prelude hiding (lookup)
import Lib (runFile', onePerLine)
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Data.List (sort, find, (\\), elemIndex)
import Data.Maybe (fromMaybe)

data Line = Line [ String ] [ String ]
  deriving Show
  
-- There's probably a better way to do this, but it's too
-- tedious to work out.
determineDigits :: [ String ] -> [ String ]
determineDigits ss = 
  let 
    hasLength n s = length s == n
    differentSegments n shorter longer = length (shorter \\ longer) == n
  in fromMaybe (error (show ss)) $ do
    one <- find (hasLength 2) ss
    four <- find (hasLength 4) ss
    seven <- find (hasLength 3) ss
    eight <- find (hasLength 7) ss

    let twothreefive = filter (hasLength 5) ss
    three <- find (differentSegments 0 one) twothreefive
    five <- find (differentSegments 1 four) (filter (/= three) twothreefive)
    two <- find (\s -> s /= three && s /= five) twothreefive

    let nice = filter (hasLength 6) ss
    nine <- find (differentSegments 0 four) nice
    zero <- find (differentSegments 0 one) (filter (/= nine) nice)
    six <- find (differentSegments 1 four) (filter (/= zero) nice)

    return [zero, one, two, three, four, five, six, seven, eight, nine]

digitsToInt :: Line -> Int
digitsToInt (Line sigs nums) = 
  let
    mappings = determineDigits sigs
    digitToInt d = fromMaybe (error d) (elemIndex d mappings)
  in
    foldl (\n d -> n * 10 + digitToInt d) 0 nums

countEasyDigits :: [ Line ] -> Int
countEasyDigits = 
  let 
    isOneFourSevenOrEight n = length n `elem` [2, 3, 4, 7]
    digits (Line _ d) = d
  in
    length . filter isOneFourSevenOrEight . (=<<) digits

sumAllNumbers :: [ Line ] -> Int
sumAllNumbers ls = sum (fmap digitsToInt ls)

-- Parser and runners

parseLines :: AP.Parser [ Line ]
parseLines = 
  let
    signal = sort <$> AP.many1 (AP.satisfy $ AP.inClass "abcdefg")
    patterns = AP.sepBy1 signal (AP.char ' ')
    line = Line <$> patterns <* AP.string " | " <*> patterns
  in
    onePerLine line

star1, star2 :: String -> IO (Either String Int)
star1 fileName = runFile' ("data/08/" <> fileName) parseLines countEasyDigits
star2 fileName = runFile' ("data/08/" <> fileName) parseLines sumAllNumbers