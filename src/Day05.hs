module Day05 where

import Lib (runFile', onePerLine, range, parseFile)
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Data.Map ( insertWith, Map, elems, keys, empty, findWithDefault )
import Codec.Picture (Image, Pixel, generateImage, Pixel8)
import Codec.Picture.Png (writePng)

type Point = (Int, Int)
type Line = (Point, Point)
type Diagram = Map Point Int

isHorizontalOrVertical :: Line -> Bool
isHorizontalOrVertical ((x1, y1), (x2, y2)) 
  = x1 == x2 || y1 == y2

isDiagonal :: Line -> Bool
isDiagonal ((x1, y1), (x2, y2))
  = abs (x1 - x2) == abs (y1 - y2)

points :: Line -> [ Point ]
points ps@((x1, y1), (x2, y2))
  | isHorizontalOrVertical ps = (,) <$> range x1 x2 <*> range y1 y2
  | isDiagonal ps             = zip (range x1 x2) (range y1 y2)
  | otherwise                 = undefined

makeDiagram :: [ Line ] -> Diagram
makeDiagram = 
  let
    updateDiagram line diagram
      = foldr (\p d -> insertWith (+) p 1 d) diagram (points line)
  in
    foldr updateDiagram empty

countIntersections :: Diagram -> Int
countIntersections = length . filter (> 1) . elems

-- This stuff isn't part of the puzzle but I wanted to see my picture

greyscale :: Int -> Int -> Pixel8 
greyscale _ 0        = 0
greyscale maxVal val = floor (fromIntegral val / fromIntegral maxVal * 255 :: Float)

makeImage :: Pixel p => (Int -> Int -> p) -> Diagram -> Image p
makeImage colourPicker diagram =
  let
    width = maximum (fst <$> keys diagram)
    height = maximum (snd <$> keys diagram)
    maxValue = maximum (elems diagram)
    colourForPixel x y = colourPicker maxValue (findWithDefault 0 (x, y) diagram)
  in
    generateImage colourForPixel width height

makeImageFromTestData :: String -> IO (Either String ())
makeImageFromTestData file = do
  linesData <- parseFile "data/05/problem.txt" parseLines
  sequence (writePng file . makeImage greyscale . makeDiagram <$> linesData)

-- Parsers and runners

parseLines :: AP.Parser [ Line ]
parseLines = 
  let 
    parsePoint = (,) <$> AP.decimal <* AP.char ',' <*> AP.decimal
    parseLine = (,) <$> parsePoint <* AP.string " -> " <*> parsePoint
  in
    onePerLine parseLine

runStar :: ([Line] -> a) -> String -> IO (Either String a)
runStar processor fileName = runFile' ("data/05/" <> fileName) parseLines processor

star1, star2 :: String -> IO (Either String Int)
star1 = runStar (countIntersections . makeDiagram . filter isHorizontalOrVertical)
star2 = runStar (countIntersections . makeDiagram)