module Day01 where

import Lib (runFile', onePerLine)
import qualified Data.Attoparsec.ByteString.Char8 as AP

-- Alternative solution brought to my attention by @spirit that avoids the
-- sum entirely since the only thing different about each consecutive
-- rolling sum is the latest number added to the sum, and the last
-- number to 'fall out' of the sum.

countIncsOffset :: Int -> [ Int ] -> Int
countIncsOffset o is = length $ filter (uncurry (<)) (zip is (drop o is))

-- Run the problem

parseInput :: AP.Parser [ Int ]
parseInput = onePerLine AP.decimal 

runProblem :: ([Int] -> b) -> IO (Either String b)
runProblem = runFile' "data/01/01.txt" parseInput

star1, star2 :: IO (Either String Int)
star1 = runProblem (countIncsOffset 1)
star2 = runProblem (countIncsOffset 3)