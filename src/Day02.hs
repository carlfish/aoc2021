module Day02 where

import Lib (runFile', onePerLine)
import qualified Data.Attoparsec.ByteString.Char8 as AP
import Control.Applicative ((<|>))
import Data.Foldable (foldl')

data Direction
  = Forward
  | Down
  | Up 

data Move = Move Direction Int

data Position = Position
  { depth :: Int
  ,  hpos :: Int
  ,   aim :: Int
  }

start :: Position
start = Position 0 0 0

move :: Position -> Move -> Position
move pos (Move Forward n) = pos { hpos = hpos pos + n }
move pos (Move Up n)      = pos { depth = depth pos - n }
move pos (Move Down n)    = pos { depth = depth pos + n }

move2 :: Position -> Move -> Position
move2 pos (Move Forward n) = pos { hpos = hpos pos + n
                                 , depth = depth pos + n * aim pos 
                                 }
move2 pos (Move Up n)      = pos { aim = aim pos - n }
move2 pos (Move Down n)    = pos { aim = aim pos + n }

getAnswer :: Position -> Int
getAnswer (Position d h _) = d * h

parseMoves :: AP.Parser [ Move ]
parseMoves = 
  let
    parseDirection
      =    Forward <$ AP.string "forward"
      <|>  Up      <$ AP.string "up"
      <|>  Down    <$ AP.string "down"
    parseMove = Move <$> parseDirection <* AP.space <*> AP.decimal
  in
    onePerLine parseMove

star1, star2 :: String -> IO (Either String Int)
star1 fileName = runFile' ("data/02/" <> fileName) parseMoves (getAnswer . foldl' move start)
star2 fileName = runFile' ("data/02/" <> fileName) parseMoves (getAnswer . foldl' move2 start)